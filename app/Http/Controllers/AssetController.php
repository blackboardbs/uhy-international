<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Client;

class AssetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getAvatar(Request $request)
    {
        if ($request->has('q') && file_exists(storage_path('app/avatars/' . $request->input('q')))) {
            return response()->file(storage_path('app/avatars/' . $request->input('q')));
        } else if(file_exists(public_path('storage/avatars/'.$request->input('q')))) {
            return response()->file(public_path('storage/avatars/' . $request->input('q')));
        } else {
            return response()->file('assets/default.png');
        }
    }

    public function getDocument(Request $request)
    {

        if (file_exists(storage_path('app/documents/' . $request->input('q')))) {
            return response()->file(storage_path('app/documents/' . $request->input('q')));
        } else if(file_exists(public_path('storage/documents/processed_applications'.$request->input('q')))) {
            return response()->file(public_path('storage/documents/processed_applications'.$request->input('q')));
        }else{
            abort(404);
        }
    }

    public function getForm(Request $request)
    {
        $client = Client::find($request->input('client'));

        if(str_contains($request->input('q'), 'CRF')){
            $type = 'CRF';
        }elseif(str_contains($request->input('q'), 'ClientRiskAssessment')){
            $type = 'CRA';
        }

        // dd(str_replace('docx','pdf',('app/forms/processedforms/'.str_replace(' ','_',(($client->company != null ? str_replace(' ','',preg_replace('/[^a-zA-Z0-9_ -]/s','',substr($client->company,0,30))) : preg_replace('/[^a-zA-Z0-9_ -]/s','',$client->first_name.'_'.str_replace(' ','_',$client->last_name))))).$request->input('q'))));
        if (file_exists(storage_path('app/forms/processedforms/'.str_replace(' ','_',(($client->company != null ? str_replace(' ','',preg_replace('/[^a-zA-Z0-9_ -]/s','',substr($client->company,0,30))) : preg_replace('/[^a-zA-Z0-9_ -]/s','',$client->first_name.'_'.str_replace(' ','_',$client->last_name))))).$request->input('q')))) {
            // return response()->file(storage_path('app/forms/processedforms/'.($client->company != null ? str_replace(' ','',preg_replace('/[^a-zA-Z0-9_ -]/s','',substr($client->company,0,30))) : preg_replace('/[^a-zA-Z0-9_ -]/s','',$client->first_name.'_'.str_replace(' ','_',$client->last_name))).$request->input('q')));
            $domPdfPath = base_path('vendor/dompdf/dompdf');
            \PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
            \PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
            
            $Content = \PhpOffice\PhpWord\IOFactory::load(storage_path('app/forms/processedforms/'.str_replace(' ','_',(($client->company != null ? str_replace(' ','',preg_replace('/[^a-zA-Z0-9_ -]/s','',substr($client->company,0,30))) : preg_replace('/[^a-zA-Z0-9_ -]/s','',$client->first_name.'_'.str_replace(' ','_',$client->last_name))))).$request->input('q'))); 
    
            $PDFWriter = \PhpOffice\PhpWord\IOFactory::createWriter($Content,'PDF');
            $PDFWriter->save(public_path(($client->company != null ? str_replace(' ','',preg_replace('/[^a-zA-Z0-9_ -]/s','',substr($client->company,0,30))) : preg_replace('/[^a-zA-Z0-9_ -]/s','',$client->first_name.'_'.str_replace(' ','_',$client->last_name))).'_'.$type.'.pdf'));

            return response()->download(public_path(($client->company != null ? str_replace(' ','',preg_replace('/[^a-zA-Z0-9_ -]/s','',substr($client->company,0,30))) : preg_replace('/[^a-zA-Z0-9_ -]/s','',$client->first_name.'_'.str_replace(' ','_',$client->last_name))).'_'.$type.'.pdf'));
        }else{
            abort(404);
        }
    }

    public function getCrf(Request $request)
    {
        if (file_exists(storage_path('app/crf/' . $request->input('q')))) {
            return response()->file(storage_path('app/crf/' . $request->input('q')));
        } else {
            abort(404);
        }
    }

    public function getTemplate(Request $request)
    {
        if (file_exists(storage_path('app/templates/' . $request->input('q')))) {
            return response()->file(storage_path('app/templates/' . $request->input('q')));
        } else {
            abort(404);
        }
    }
}
