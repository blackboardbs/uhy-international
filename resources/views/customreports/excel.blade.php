<html>
<head>
    <title>Custom Report</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        html,body{
            width: 100%;
        }
        table{
            border-collapse: collapse;
            width: 100%;
        }

        table, th, td {
            border: 1px solid black;
            padding: 5px;
            font-size:12px;
            word-break: break-all;
            overflow-wrap: break-word;
        }

        th{
            background:#ccc;
        }

        table tr td, table tr th { page-break-inside: avoid; }
    </style>
</head>
<body>
<button hidden id="excelExport" class="btn btn-info btn-sm" onclick="ExportToExcel('xlsx')"><i class="fas fa-file-excel"></i> Excel</button>
<div class="table-responsive">
    <table id="reportTable" class="table table-bordered table-sm table-hover" style="border: 1px solid #dee2e6;display: table;border-collapse: collapse">
        <thead class="btn-dark">
        <tr>
            <th>Name</th>
            <th>Process Stage</th>
            @foreach($fields as $key => $val)
                @if($val != null)
                    <th>{{$val}}</th>
                @endif
            @endforeach
        </tr>
        </thead>
        <tbody>
        @forelse($clients as $client)
            @if(isset($client['id']))
            <tr>
                @foreach ($step_names as $step_name)
                    @if ($step_name->id == $client['step_id'] && $step_name->process_id == $client['process_id'])
                    <tr>
                        <td class="table100-firstcol"><a href="{{route('clients.overview',[$client["id"],$client["process_id"],$client["step_id"]])}}">{{$client['company']}}</a></td>
                        <td><a href="{{route('clients.show',$client["id"])}}">{{$step_name->name}}</a></td>
                        @foreach($client["data"] as $key => $val)
                            <td><a href="{{route('clients.show',$client["id"])}}">@if($val != strip_tags($val)) {!! $val !!} @else {{$val}} @endif</a></td>
                        @endforeach
                        @break
                    </tr>
                    @endif
                @endforeach
            </tr>
            @endif
        @empty
            <tr>
                <td colspan="100%" class="text-center"><small class="text-muted">No clients match those criteria.</small></td></td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>
</body>
</html>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/xlsx@0.15.1/dist/xlsx.full.min.js"></script>
<script>
    $(document).ready(function () {
        $('#excelExport').click();
        history.back();
    });
    function ExportToExcel(type, fn, dl) {
        let today = new Date().toLocaleDateString()
        var reportName = 'CustomReport-'+today;
        var elt = document.getElementById('reportTable');
        var wb = XLSX.utils.table_to_book(elt, { sheet: "sheet1" });
        return dl ?
            XLSX.write(wb, { bookType: type, bookSST: true, type: 'base64' }):
            XLSX.writeFile(wb, fn || (reportName + '.' + (type || 'xlsx')));
    }
</script>
