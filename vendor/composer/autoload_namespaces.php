<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'UpdateHelper\\' => array($vendorDir . '/kylekatarnls/update-helper/src'),
    'ShvetsGroup\\LaravelEmailDatabaseLog\\' => array($vendorDir . '/shvetsgroup/laravel-email-database-log/src'),
    'Sentry\\SentryLaravel\\' => array($vendorDir . '/sentry/sentry-laravel/src'),
    'Raven_' => array($vendorDir . '/sentry/sentry/lib'),
    'Parsedown' => array($vendorDir . '/erusev/parsedown'),
    'HTMLPurifier' => array($vendorDir . '/ezyang/htmlpurifier/library'),
);
